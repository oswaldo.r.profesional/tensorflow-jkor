<<<<<<< HEAD
<p align="center">
  <img src=".github/object-detection.jpg" alt="Nest Logo" />
</p>

## Descripción

Esta aplicación detecta objetos utilizando TensorFlow.js. El modelo CocoSSD ya está entrenado y cuenta con 80 clases diferentes para la detección, como Persona, Gato, Perro, Teléfono Celular, Silla, entre otros.

Para ejecutar esta aplicación, es necesario contar con una cámara web para capturar el video. El proceso de detección es en tiempo real, y para mostrar la forma geométrica se utilizó el elemento Canvas de HTML.

## Instalar dependencias

```bash
$ npm install
```

Si encuentras algún error durante el proceso de instalación, ejecuta el siguiente comando:

```bash
$ npm install --legacy-peer-deps
```

## Ejecución de la aplicación

```bash
# Development mode
$ npm run dev

### Now access app:

- **Development mode**: http://localhost:3000
